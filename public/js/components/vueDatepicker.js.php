<?php

if(file_exists("../../../vendor/vuejs/vuedatepicker_js/vue-datepicker.iife.js"))
{
	header('Content-Type: application/javascript');
	echo file_get_contents("../../../vendor/vuejs/vuedatepicker_js/vue-datepicker.iife.js");
	echo "export default VueDatePicker";
}
else
{
	header('HTTP/1.0 404 Not Found');
}
